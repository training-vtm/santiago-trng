package com.quirumbay.taski;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaskiApplication.class, args);
	}

}
