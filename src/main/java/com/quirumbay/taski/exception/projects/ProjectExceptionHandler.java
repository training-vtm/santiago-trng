package com.quirumbay.taski.exception.projects;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ProjectExceptionHandler {

    @ExceptionHandler(value = {ProjectNotFoundException.class})
    public ResponseEntity<Object> handleProjectNotFoundException(
            ProjectNotFoundException userNotFoundException
    ){
        ProjectException userException = new ProjectException(
                userNotFoundException.getMessage(),
                userNotFoundException.getCause(),
                HttpStatus.NOT_FOUND
        );

                return new ResponseEntity<>(userException, HttpStatus.NOT_FOUND);

    }



}
