package com.quirumbay.taski.exception.projects;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Setter
@Getter
@AllArgsConstructor

public class ProjectException {

    private final String message;
    private final Throwable throwable;
    private final HttpStatus httpStatus;

}
