package com.quirumbay.taski.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Setter
@Getter
@AllArgsConstructor

public class UserException {

    private final String message;
    private final Throwable throwable;
    private final HttpStatus httpStatus;

}
