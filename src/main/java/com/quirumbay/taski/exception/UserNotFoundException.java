package com.quirumbay.taski.exception;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;


public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(String message) {
        super(message);
    }

    public UserNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

}
