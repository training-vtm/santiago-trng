package com.quirumbay.taski.controllers;

import com.quirumbay.taski.services.TasksService;
import com.quirumbay.taski.entities.dto.CreateTaskDto;
import com.quirumbay.taski.entities.Task;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tasks")
@RequiredArgsConstructor
public class TasksController {


    private final TasksService tasksService;

    @GetMapping
    public List<Task> findAll() {
        return tasksService.findAll();
    }

    @PostMapping
    public Task create(@RequestBody @Valid CreateTaskDto createTaskDto ){

        return tasksService.create(createTaskDto);
    }

    @GetMapping("{id}")
    public Task findOne(@PathVariable Long id){
        return tasksService.findOne(id);
    }

    @PutMapping("{id}")
    public Task update(@PathVariable Long id, @RequestBody @Valid Task task){
        return tasksService.update(id, task);
    }

    @DeleteMapping("{id}")
    public void remove(@PathVariable Long id){
        tasksService.remove(id);
    }



}
