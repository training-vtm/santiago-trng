package com.quirumbay.taski.controllers;


import com.quirumbay.taski.entities.dto.CreateProjectDto;
import com.quirumbay.taski.entities.Project;
import com.quirumbay.taski.services.ProjectsService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/projects")
@RequiredArgsConstructor
public class ProjectsController {

    private final ProjectsService projectsService;

    @PostMapping()
    public Project create(@RequestBody @Valid CreateProjectDto createProjectDto){
        return projectsService.create(createProjectDto);

    }

    @GetMapping
    public List<Project> findAll(){

        return projectsService.findAll();
    }

    @GetMapping("{id}")
    public Project findOne(@PathVariable Long id){
        return projectsService.findOne(id);
    }

    @PutMapping("{id}")
    public Project update(@PathVariable Long id, @RequestBody @Valid Project project){
        return projectsService.update(id, project);
    }


    @DeleteMapping("{id}")
    public void remove(@PathVariable Long id){
        projectsService.remove(id);
    }







}
