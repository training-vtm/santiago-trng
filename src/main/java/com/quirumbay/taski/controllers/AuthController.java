package com.quirumbay.taski.controllers;

import com.quirumbay.taski.entities.dto.auth.AuthResponseDto;
import com.quirumbay.taski.entities.dto.auth.LoginRequestDto;
import com.quirumbay.taski.entities.dto.auth.RegisterUserDto;
import com.quirumbay.taski.services.AuthService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

    @PostMapping(value = "login")
    public ResponseEntity<AuthResponseDto> login(@RequestBody LoginRequestDto loginRequestDto)
    {
        AuthResponseDto authResponse = authService.login(loginRequestDto);

        return ResponseEntity.ok(authResponse);
    }

    @PostMapping(value = "register")
    public ResponseEntity<AuthResponseDto> register(@RequestBody @Valid RegisterUserDto registerUserDto){

        AuthResponseDto authResponse = authService.register(registerUserDto);

        return ResponseEntity.status(HttpStatus.CREATED).body(authResponse);
    }





}
