package com.quirumbay.taski.controllers;

import com.quirumbay.taski.services.TagsService;
import com.quirumbay.taski.entities.dto.CreateTagDto;
import com.quirumbay.taski.entities.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tags")
@RequiredArgsConstructor
public class TagsController {

    private final TagsService tagsService;

    @GetMapping
    public List<Tag> findAll(){
        return tagsService.findAll();
    }

    @GetMapping("{id}")
    public Tag findOne(@PathVariable  Long id){

        return tagsService.findOne(id);

    }

    @PostMapping()
    public Tag create(@RequestBody @Valid CreateTagDto createTagDto){
        return tagsService.create(createTagDto);
    }

    @PutMapping("{id}")
    public Tag update(@PathVariable Long id, @RequestBody @Valid Tag tag){
        return tagsService.update(id, tag);
    }

    @DeleteMapping("{id}")
    public void remove(@PathVariable Long id){
        tagsService.remove(id);
    }




}
