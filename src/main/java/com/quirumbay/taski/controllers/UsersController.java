package com.quirumbay.taski.controllers;

import com.quirumbay.taski.services.UsersService;
import com.quirumbay.taski.entities.User;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@Validated
@RequiredArgsConstructor
public class UsersController {


    private final UsersService usersService;

    @GetMapping()
    public List<User> findAll() {

        return usersService.findAll();

    }

    @GetMapping("{id}")
    public User findOne(@PathVariable Long id) {
        return usersService.findOne(id);
    }


    /*
    @PutMapping("{id}")
    public User findOne(@RequestBody @Valid RegisterUserDto registerUserDto, @PathVariable() Long id){
    }
     */

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove (@PathVariable Long id) {
        usersService.remove(id);
    }



}
