package com.quirumbay.taski.services;

import com.quirumbay.taski.repositories.UsersRepository;
import com.quirumbay.taski.entities.User;
import com.quirumbay.taski.exception.UserNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UsersService {

    private final UsersRepository usersRepository;

    public List<User> findAll() {
        return usersRepository.findAll();
    }

    public User findOne(Long id) {

        if(usersRepository.findById(id).isEmpty())
            throw new UserNotFoundException("Usuario con el id " + id + " no encontrado");

        return usersRepository.findById(id).get();
    }


    public User update(Long id, User user) {

        User userToUpdate = findOne(id);

        return usersRepository.save(user);

    }

    public void remove(Long id) {
        User userToRemove = findOne(id);

        userToRemove.setActive(false);

        usersRepository.save(userToRemove);

        //usersRepository.deleteById(id);
    }

}
