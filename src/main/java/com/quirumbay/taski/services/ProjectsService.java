package com.quirumbay.taski.services;


import com.quirumbay.taski.repositories.ProjectRepository;
import com.quirumbay.taski.entities.dto.CreateProjectDto;
import com.quirumbay.taski.entities.Project;
import com.quirumbay.taski.exception.projects.ProjectNotFoundException;
import com.quirumbay.taski.entities.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProjectsService {

    private final ProjectRepository projectRepository;

    private final UsersService usersService;

    public List<Project> findAll() {
        return projectRepository.findAll();
    }


    public Project findOne(Long id) {

        if(projectRepository.findById(id).isEmpty()){
            throw new ProjectNotFoundException("El proyecto con el id " + id + " no encontrado");
        }

        return projectRepository.findById(id).get();

    }



    public Project create(CreateProjectDto createProjectDto) {

        Long userId = createProjectDto.getUserId();

        User user = usersService.findOne(userId);

        Project projectToCreate = new Project(null, createProjectDto.getTitle(), user, null);

        user.getProjects().add(projectToCreate);

        usersService.update(user.getId(), user);

        return projectRepository.save(projectToCreate);

    }


     public Project update(Long id, Project project) {
            Project projectToUpdate = findOne(id);

            if(project.getTitle() != null){
                projectToUpdate.setTitle(project.getTitle());
            }

            if(project.getTasks() != null){
                projectToUpdate.setTasks(project.getTasks());
            }

            if (project.getUser() != null){
                projectToUpdate.setUser(project.getUser());
            }

            return projectRepository.save(projectToUpdate);
     }


     public void remove(Long id){
            Project projectToRemove = findOne(id);

         System.out.println("proyecto a eliminar " + projectToRemove.getId());

            projectRepository.delete(projectToRemove);
     }
}
