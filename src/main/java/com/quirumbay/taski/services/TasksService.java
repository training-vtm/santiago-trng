package com.quirumbay.taski.services;


import com.quirumbay.taski.entities.Project;
import com.quirumbay.taski.entities.Tag;
import com.quirumbay.taski.repositories.TaskRepository;
import com.quirumbay.taski.entities.dto.CreateTaskDto;
import com.quirumbay.taski.entities.Task;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class TasksService implements ITaskService {

    private final TaskRepository taskRepository;

    private final ProjectsService projectsService;

    private final TagsService tagsService;

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task findOne(Long id) {

        if(taskRepository.findById(id).isEmpty()){

        }
        return taskRepository.findById(id).get();

    }

    @Override
    public Task create(CreateTaskDto createTaskDto) {

        Long projectId = createTaskDto.getProjectId();

        Project project = projectsService.findOne(projectId);

        Task taskToCreate = new Task();

        Set<Tag> tags = new HashSet<>();

        taskToCreate.setTitle(createTaskDto.getTitle());
        taskToCreate.setDescription(createTaskDto.getDescription());

        if (createTaskDto.getStatus() != null){
            taskToCreate.setStatus(createTaskDto.getStatus());
        }

        if(createTaskDto.getPriority() != null) {
            taskToCreate.setPriority(createTaskDto.getPriority());
        }

        if(createTaskDto.getDueDate() != null){
            taskToCreate.setDueDate(createTaskDto.getDueDate());
        }

        taskToCreate.setProject(project);

        for (Long tagId : createTaskDto.getTagsIds()) {
            Tag tag = tagsService.findOne(tagId);
            if (tag != null) {
                tags.add(tag);
            }
        }

        taskToCreate.setTags(tags);

        Task createdTask = taskRepository.save(taskToCreate);

        project.getTasks().add(createdTask);

        projectsService.update(project.getId(), project);

        return createdTask;
    }

    @Override
    public Task update(Long id, Task task) {

        Task taskToUpdate = findOne(id);

        if(task.getTitle() != null){
            taskToUpdate.setTitle(task.getTitle());
        }

        if(task.getDescription() != null){
            taskToUpdate.setDescription(task.getDescription());
        }

        if(task.getStatus() != null){
            taskToUpdate.setStatus(task.getStatus());
        }

        if(task.getPriority() != null){
            taskToUpdate.setPriority(task.getPriority());
        }

        if(task.getDueDate() != null){
            taskToUpdate.setDueDate(task.getDueDate());
        }

        if(task.getTags() != null){
            taskToUpdate.setTags(task.getTags());
        }

        if(task.getProject() != null){
            taskToUpdate.setProject(task.getProject());
        }

        return taskRepository.save(taskToUpdate);
    }

    @Override
    public void remove(Long id) {

        Task taskToRemove = findOne(id);

        System.out.println("taskToRemove: " + taskToRemove);

        taskRepository.deleteById(taskToRemove.getId());

    }
}
