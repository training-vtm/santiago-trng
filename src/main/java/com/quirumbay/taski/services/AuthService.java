package com.quirumbay.taski.services;

import com.quirumbay.taski.entities.dto.auth.AuthResponseDto;
import com.quirumbay.taski.entities.dto.auth.LoginRequestDto;
import com.quirumbay.taski.entities.dto.auth.RegisterUserDto;
import com.quirumbay.taski.jwt.JwtService;
import com.quirumbay.taski.repositories.UsersRepository;
import com.quirumbay.taski.entities.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final AuthenticationManager authenticationManager;

    private final JwtService jwtService;

    private final UsersRepository usersRepository;

    private final PasswordEncoder passwordEncoder;

    public AuthResponseDto login(LoginRequestDto loginRequestDto) {

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequestDto.getUsername(), loginRequestDto.getPassword()));

        UserDetails userDetails = usersRepository.findByUsername(loginRequestDto.getUsername()).orElseThrow();

        User user = usersRepository.findByUsername(loginRequestDto.getUsername()).orElseThrow();

        String token = jwtService.getToken(userDetails);

        return AuthResponseDto.builder()
                .token(token)
                .user(user)
                .build();
    }

    public AuthResponseDto register(RegisterUserDto registerUserDto) {

        String password = passwordEncoder.encode(registerUserDto.getPassword());

        User userToCreate = new User(
                null,
                registerUserDto.getFirstName(),
                registerUserDto.getLastName(),
                registerUserDto.getUsername(),
                password,
                registerUserDto.getEmail(),
                null,
                true

                );

        User createdUser = usersRepository.save(userToCreate);

        String token = jwtService.getToken(createdUser);

        return AuthResponseDto.builder()
                .token(token)
                .user(createdUser)
                .build();


    }
}
