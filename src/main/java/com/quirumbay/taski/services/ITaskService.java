package com.quirumbay.taski.services;

import com.quirumbay.taski.entities.dto.CreateTaskDto;
import com.quirumbay.taski.entities.Task;

import java.util.List;

public interface ITaskService {

    public List<Task> findAll();

    public Task findOne(Long id);

    public Task create(CreateTaskDto createTaskDto);

    public Task update(Long id, Task task);

    public void remove(Long id);
}
