package com.quirumbay.taski.services;

import com.quirumbay.taski.repositories.TagsRepository;
import com.quirumbay.taski.entities.dto.CreateTagDto;
import com.quirumbay.taski.entities.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TagsService {

    private final TagsRepository tagsRepository;

    public List<Tag> findAll() {

        return tagsRepository.findAll();
    }

    public Tag findOne(Long id) {

        if(tagsRepository.findById(id).isEmpty()){

        }
        return tagsRepository.findById(id).get();

    }

    public Tag create(CreateTagDto createTagDto) {

        Tag tagToCreate = new Tag(null, createTagDto.getName());

        return tagsRepository.save(tagToCreate);

    }

    public Tag update(Long id, Tag tag) {
        Tag tagToUpdate = findOne(id);
        return tagsRepository.save(tag);
    }

    public void remove(Long id){

        Tag tagToDelete = findOne(id);


        tagsRepository.deleteById(id);
    }

}
