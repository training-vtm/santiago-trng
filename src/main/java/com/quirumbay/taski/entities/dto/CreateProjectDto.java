package com.quirumbay.taski.entities.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateProjectDto {

    @NotNull(message = "El nombre del proyecto es necesario")
    private String title;

    @NotNull(message = "El usuario es necesario")
    @Min(value = 1, message = "El id del usuario no es válido")
    private Long userId;
}
