package com.quirumbay.taski.entities.dto.auth;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterUserDto {

    @NotNull(message = "El nombre es requerido")
    private String firstName;

    @NotNull(message = "El apellido es requerido")
    private String lastName;

    @NotNull(message = "El email es requerido")
    @Email(message = "email debe ser un email válido")
    private String email;

    @NotNull(message = "La contraseña es requerida")
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{6,}$",
            message = "La contraseña debe tener al menos 6 caracteres, una letra mayúscula, una letra minúscula y un número")
    private String password;

    @NotNull(message = "El nombre de usuario es requerido")
    private String username;


}
