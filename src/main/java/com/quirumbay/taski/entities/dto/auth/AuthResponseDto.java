package com.quirumbay.taski.entities.dto.auth;


import com.quirumbay.taski.entities.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class AuthResponseDto {
    String token;
    User user;
}
