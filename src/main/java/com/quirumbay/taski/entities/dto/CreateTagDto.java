package com.quirumbay.taski.entities.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CreateTagDto {

    @NotNull(message = "El nombre del tag es requerido")
    private String name;


}
