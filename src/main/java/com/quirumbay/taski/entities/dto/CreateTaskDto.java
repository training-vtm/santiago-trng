package com.quirumbay.taski.entities.dto;

import com.quirumbay.taski.entities.enums.Priority;
import com.quirumbay.taski.entities.enums.Status;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateTaskDto {

    @NotNull(message = "El titulo de la tarea es requerido")
    private String title;

    private String description;

    @Enumerated(EnumType.STRING)
    private Priority priority;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Temporal(TemporalType.TIMESTAMP)
    private Timestamp dueDate;

    @NotNull(message = "El proyecto es necesario")
    @Min(value = 1, message = "El id del proyecto no es válido")
    private Long projectId;

    @Size(min = 1, message = "Debes proporcionar al menos una etiqueta")
    private List<Long> tagsIds;


}
