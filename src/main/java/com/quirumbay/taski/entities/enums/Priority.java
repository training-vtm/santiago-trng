package com.quirumbay.taski.entities.enums;

public enum Priority {

    LOW,
    MEDIUM,
    HIGH

}
