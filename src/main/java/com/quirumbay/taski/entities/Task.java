package com.quirumbay.taski.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.quirumbay.taski.common.entities.AuditableBase;
import com.quirumbay.taski.entities.enums.Priority;
import com.quirumbay.taski.entities.enums.Status;
import jakarta.persistence.*;
import lombok.*;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tasks")
public class Task extends AuditableBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String title;

    private String description;

    @Enumerated(EnumType.STRING)
    private Priority priority = Priority.LOW;

    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Temporal(TemporalType.TIMESTAMP)
    private Timestamp dueDate  = new Timestamp(System.currentTimeMillis());

    @JsonIgnore
    @ManyToOne(
            optional = false
    )
    @JoinColumn(name = "project_id")
    private Project project;

    @ManyToMany(
            fetch = FetchType.EAGER

    )
    @JoinTable(
            name = "task_has-tags",
            joinColumns = {
                     @JoinColumn(name = "fk_task")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "fk_tags")
            }
    )
    private Set<Tag> tags = new HashSet<>();



}
